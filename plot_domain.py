import matplotlib.pyplot as plt

def plotXY(fx, fy, umin, umax, vmin, vmax, step=1):
    '''
    Esta função plota um dominio retangular de integração no plano xy 
    a partir de curvas parametrizadas.

    Parametros

        fx e fx: são funções que parametrizam 
        respectivamente os pontos de x e y
        
        umin e vmin: são os inferiores do dominio de u e v
        sen uv o plano parametrizado

        umax e vmax: são os superiores do dominio de u e v
        sen uv o plano parametrizado

        step: o passo com que varia o intervalo do plot
    '''
    x = []
    y = []
    for i in range(100*umin, (100*umax)+1, step):
        for j in range(100*vmin, (100*vmax)+1, step):
            u = i/100.0
            v = j/100.0
            x.append(fx(u,v))
            y.append(fy(u,v))

    plt.scatter(x, y, alpha=0.5)
    plt.show()
    
def plot_scatter(x, y):
    '''
    Esta função plota um dominio de integração no plano xy 
    a partir de curvas parametrizadas.

    Parametros

    '''
    plt.scatter(x, y, alpha=0.5)
    plt.show()

def define_region(fx, fy, xmin, xlimit, ymin, ylimit, flimit_condition):
    x, y = [], []
    for i in range(int(100*xmin), int(100*xlimit)+1):
        for j in range(int(100*ymin), int(100*ylimit)+1):
            u = i/100
            v = j/100
            if(flimit_condition(u,v)):
                x.append(fx(u,v))
                y.append(fy(u,v))

    return {"x": x, "y": y}







