from plot_domain import plot_scatter, define_region

def f_u(y,x):
    return y+2*x

def f_v(y,x):
    return y-2*x+1

def lines(x,y):
    return y - 2*x <= 2 and y + 2*x <= 2 and y - 2*x >= 1 and y + 2*x >= 1 

xy = define_region(f_u, f_v, -0.25, 0.25, 1, 2, lines)
plot_scatter(xy["x"], xy["y"])


