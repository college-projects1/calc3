from plot_domain import plotXY

def f_x(u,v):
    return u*v

def f_y(u,v):
    return v-u

plotXY(f_x, f_y, 0, 1, 1, 2)

