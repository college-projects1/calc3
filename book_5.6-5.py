from plot_domain import plot_scatter, define_region

def f_u(x,y):
    return (x**2)-y

def f_v(x,y):
    return x+y

def lines(x,y):
    return y >= x**2 and y <= (x**2)+1 and y + x >= 1 and y + x <= 2

xy = define_region(f_u, f_v, 0, 2, 0, 2, lines)
plot_scatter(xy["x"], xy["y"])


