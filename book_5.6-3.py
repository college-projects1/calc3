from plot_domain import plot_scatter, define_region

def f_u(x,y):
    return x-y

def f_v(x,y):
    return x+y

def lineXY(x,y):
    return x+y <= 1


xy = define_region(f_u, f_v, 0, 1, 0, 1, lineXY)
plot_scatter(xy["x"], xy["y"])


