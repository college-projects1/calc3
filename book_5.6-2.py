from plot_domain import plot_scatter, define_region

def f_x(u,v):
    return u+v

def f_y(u,v):
    return v-(u**2)

def lineUV(u,v):
    return u+v <= 2


xy = define_region(f_x, f_y, 0, 2, 0, 2, lineUV)
plot_scatter(xy["x"], xy["y"])


